(function () {
    const numberSquares = (n) => {
        if (Number(n)) {
            return (n * (n + 1) * (2 * n + 1)) / 6;
        } else {
            return;
        }
    };
})();
